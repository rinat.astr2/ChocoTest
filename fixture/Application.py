import allure
from allure_commons.types import AttachmentType
from selenium import webdriver

from Pages.homePage import HomePageHelper
#


class Application:

    def __init__(self):
        capabilities = {
            "browserName": "chrome",
            "browserVersion": "90",
            "selenoid:options": {
                "enableVNC": True,
                "enableVideo": True
            }
        }
        driver = webdriver.Remote(
            command_executor="http://localhost:4444/wd/hub",
            desired_capabilities=capabilities)
        self.driver = driver
        self.driver.implicitly_wait(60)
        self.driver.maximize_window()
        self.homePage = HomePageHelper(self)

    def destroy(self):
        allure.attach(self.driver.get_screenshot_as_png(), attachment_type=AttachmentType.PNG)
        self.driver.quit()
