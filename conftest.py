import pytest


from fixture.Application import Application


@pytest.fixture()
def app(request):
    fixture = Application()
    request.addfinalizer(fixture.destroy)
    return fixture

# @pytest.fixture(scope="session")
# def app(request):
#    driver = webdriver.Chrome()
#    driver.implicitly_wait(60)
#    request.addfinalizer(driver.quit)
#    return driver

# @pytest.fixture(scope="session")
# def browser():
#    driver = webdriver.Chrome()
#    yield driver
#    driver.quit()
