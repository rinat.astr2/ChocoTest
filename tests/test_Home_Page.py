# -*- coding: utf-8 -*-
import allure
from allure_commons.types import AttachmentType


#@allure.title("Поиск города без значений")
#def test_find_tickets_without_value(app):
#    app.homePage.open_home_page()
#    app.homePage.click_find_tickets_button()
#    assert app.driver.find_element_by_css_selector("div.win-block").is_displayed()
    # allure.attach('screenshot', app.driver.get_screenshot_as_png(), type=AttachmentType.PNG)


@allure.title("Удаление города из поля 'Город отправления'")
def test_delete_value_departure_city(app):
    app.homePage.open_home_page()
    app.homePage.delete_departure_city_value(city_1="Алм")
    assert app.driver.find_element_by_id("city_1").get_attribute("value") == ""


@allure.title("Удаление города из поля 'Город прибытия'")
def test_delete_value_arrival_city(app):
    app.homePage.open_home_page()
    app.homePage.delete_arrival_city_value(city_2="Аст")
    assert app.driver.find_element_by_id("city_2").get_attribute("value") == ""


@allure.title("Смена городов с помощью кнопки 'Switch Cities'")
def test_switch_city_with_values(app):
    app.homePage.open_home_page()
    app.homePage.set_departure_city_value(city_1="Алм")
    app.homePage.set_arrival_city_value(city_2="Аст")
    app.homePage.switch_city()
    assert app.driver.find_element_by_id("city_2").get_attribute("value") == "ALA"
    assert app.driver.find_element_by_id("city_1").get_attribute("value") == "NQZ"


@allure.title("Обычный поиск")
def test_find_tickets_with_value(app):
    app.homePage.open_home_page()
    app.homePage.set_departure_city_value(city_1="Алм")
    app.homePage.set_arrival_city_value(city_2="Аст")
    app.homePage.set_date_value()
    app.homePage.click_find_tickets_button()
    assert app.driver.find_element_by_id("city_1").get_attribute("value") == "ALA"
    assert app.driver.find_element_by_id("city_2").get_attribute("value") == "NQZ"

# def test_find_tickets_with_popular_destinations(app):
# app.open_home_page()
# app.set_popular_destinations()
# assert app.driver.find_element_by_link_text("Нур-Султан").is_displayed(False)
