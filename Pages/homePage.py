import allure


def departure_city_value(city_1, driver):
    driver.find_element_by_id("city_1_user").click()
    driver.find_element_by_id("city_1_user").clear()
    driver.find_element_by_id("city_1_user").send_keys(city_1)
    driver.find_element_by_xpath("//div[@id='city_result_1']/ul/li").click()


def arrival_city_value(city_2, driver):
    driver.find_element_by_id("city_2_user").click()
    driver.find_element_by_id("city_2_user").clear()
    driver.find_element_by_id("city_2_user").send_keys(city_2)
    driver.find_element_by_xpath("//div[@id='city_result_2']/ul/li/div/span").click()


class HomePageHelper:

    def __init__(self, app):
        self.app = app

    def open_home_page(self):
        driver = self.app.driver
        with allure.step("Открыть главную страницу"):
            driver.get("http://chocotravel-test.apps.chocotravel.com/")

    def set_departure_city_value(self, city_1):
        driver = self.app.driver
        with allure.step("Ввести город отправления"):
            departure_city_value(city_1, driver)

    def set_arrival_city_value(self, city_2):
        driver = self.app.driver
        with allure.step("Ввести город прибытия"):
            arrival_city_value(city_2, driver)

    def set_date_value(self):
        driver = self.app.driver
        with allure.step("Указываем дату полета(туда)"):
            driver.find_element_by_xpath("//*[@id='calen']/div/div[4]/div[3]/span[25]").click()

    def click_find_tickets_button(self):
        driver = self.app.driver
        with allure.step("Нажимаем на кнопку 'Найти билеты'"):
            driver.find_element_by_xpath("(//button[@type='button'])[5]").click()
            driver.refresh()

    def delete_departure_city_value(self, city_1):
        driver = self.app.driver
        with allure.step("Удаляем город отправления"):
            departure_city_value(city_1, driver)
            driver.find_element_by_xpath("//form[@id='search_form']/div/div[5]/div/div").click()

    def delete_arrival_city_value(self, city_2):
        driver = self.app.driver
        with allure.step("Удаляем город прибытия"):
            arrival_city_value(city_2, driver)
            driver.find_element_by_xpath("//form[@id='search_form']/div/div[5]/div[2]/div").click()

    def switch_city(self):
        driver = self.app.driver
        with allure.step("Переключатель городов"):
            driver.find_element_by_xpath("//form[@id='search_form']/div/div[5]/button/div").click()

    def set_popular_destinations(self):
        driver = self.app.driver
        with allure.step("Популярные направления"):
            driver.find_element_by_id("city_1_user").clear()
            driver.find_element_by_id("city_2_user").clear()
        # driver.find_element_by_link_text("Нур-Султан").click()
        # driver.find_element_by_link_text("Шымкент").click()

